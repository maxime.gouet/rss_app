package com.example.rss_app;

import java.util.ArrayList;

public class Item {
    private String title;
    private String Date;
    private String Description;

    public Item(String title, String date, String description) {
        this.title = title;
        Date = date;
        Description = description;
    }
    public Item() {
        this.title = "";
        Date = "";
        Description = "";
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return Date;
    }

    public String getDescription() {
        return Description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
