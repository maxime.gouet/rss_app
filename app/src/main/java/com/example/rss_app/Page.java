package com.example.rss_app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.jar.Attributes;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Page extends Activity {

    int index = 0;
    ArrayList<Item> listitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page);

        MyRSSsaxHandler handler = new MyRSSsaxHandler();
        handler.setUrl("https://www.lemonde.fr/international/rss_full.xml ");
        Toast.makeText(this,"chargement image :"+handler.getNumber(), Toast.LENGTH_LONG).show();
        new DownloadRssTask(this).execute(handler);
    }
    public void resetDisplay(ArrayList<Item> list){
        listitem = list;
        updateDisplay();
    }

    public void updateDisplay(){
        TextView Titre = findViewById(R.id.imageTitle);
        Titre.setText(listitem.get(index).getTitle());

        TextView Date = findViewById(R.id.imageDate);
        Date.setText(listitem.get(index).getDate());

        TextView IDesc = findViewById(R.id.imageDescription);
        IDesc.setText(listitem.get(index).getDescription());

        /*ImageView Image = findViewById(R.id.imageDisplay);
        Image.setImageBitmap(image);*/
    }

    public void precedent(View view) {
        index--;
        updateDisplay();
    }

    public void suivant(View view) {
        index++;
        updateDisplay();
    }
}
