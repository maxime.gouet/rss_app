package com.example.rss_app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.xml.sax.Attributes;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MyRSSsaxHandler extends DefaultHandler {

    private String url = null ;// l'URL du flux RSS à parser
    // Ensemble de drapeau permettant de savoir où en est le parseur dans le flux XML
    private boolean inTitle = false ;
    private boolean inDescription = false ;
    private boolean inItem = false ;
    private boolean inDate = false ;
    // L'image référencée par l'attribut url du tag <enclosure>
    private Bitmap image = null ;
    private String imageURL = null ;
    // Le titre, la description et la date extraits du flux RSS
    private StringBuffer title = new StringBuffer();
    private StringBuffer description = new StringBuffer();
    private StringBuffer date = new StringBuffer();
    private int numItem = 0; // Le numéro de l'item à extraire du flux RSS
    private int numItemMax = - 1; // Le nombre total d’items dans le flux RSS
    ArrayList<Item> listItem;
    Item currentItem;

    public ArrayList<Item> getListItem() {
        return listItem;
    }

    public void setUrl(String url){
        this.url= url;
    }

    public void processFeed(){
        try {
            listItem = new ArrayList<Item>();
            numItem = 0; //A modifier pour visualiser un autre item
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader reader = parser.getXMLReader();
            reader.setContentHandler(this);
            InputStream inputStream = new URL(url).openStream();
            reader.parse(new InputSource(inputStream));
            image = getBitmap(imageURL);
            numItemMax = numItem;
        }catch(Exception e) {
            Log.e("smb116rssview", "processFeed Exception" + e.getMessage());
        }
    }
    public Bitmap getBitmap(String imageURL) {
        try {
            URL url = new URL(imageURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //  connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(localName.equals("title")){
            inTitle = true;
        }else if(localName.equals("description")){
            inDescription = true;
        }else if(localName.equals("pubDate")){
            inDate = true;
        }else if(localName.equals("item")){
            inItem= true;
            currentItem = new Item();
        }
    }
    @Override
    public void endElement (String uri,String localName,String qName){
        if(localName.equals("title")){
            inTitle = false;
        }else if(localName.equals("description")){
            inDescription = false;
        }else if(localName.equals("pubDate")){
            inDate = false;
        }else if(localName.equals("item")){
            inItem = false;
            listItem.add(currentItem);
        }
    }
    public void characters(char ch[], int start, int length){
        String chars = new String(ch).substring(start, start+length);
        if(inItem){
            if(inTitle){
                currentItem.setTitle(chars);
            }else if(inDescription){
                currentItem.setDate(chars);
            }else if(inDate){
                currentItem.setDate(chars);
            }
        }
    }
    public int getNumber() {
        return numItem;
    }
    public StringBuffer getTitle() {
        return title;
    }
    public StringBuffer getDate() {
        return date;
    }
    public Bitmap getImage() {
        return image;
    }
    public StringBuffer getDescription() {
        return description;
    }
}
