package com.example.rss_app;

import android.os.AsyncTask;
import android.util.Log;

import com.example.rss_app.MyRSSsaxHandler;

import org.jetbrains.annotations.NotNull;

public class DownloadRssTask extends AsyncTask<MyRSSsaxHandler, Void, MyRSSsaxHandler> {
    private Page monActivity;

    public DownloadRssTask(Page monActivity){
        this.monActivity = monActivity;
    }

    protected final MyRSSsaxHandler doInBackground(MyRSSsaxHandler... handler){
        handler[0].processFeed();
        return handler[0];
    }
    /** The system calls this to perform work in the UI thread and delivers
     * the result from doInBackground() */
    protected void onPostExecute(@NotNull MyRSSsaxHandler handler)
    {
        monActivity.resetDisplay(handler.listItem);
    }
}

